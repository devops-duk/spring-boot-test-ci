package com.spring.test.ci.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;


@Data
@ToString
public class ResponseDto<T> {
    boolean success;
    String errMsg;
    T data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
