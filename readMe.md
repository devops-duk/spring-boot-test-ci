### 运行单元测试
- mvn clean install -P unit -s settings.xml

### 运行集成测试测试
- mvn clean install -P int -s settings.xml

### 运行所有测试
- mvn clean install -P tests -s settings.xml

### 忽略测试构建
- mvn clean install -P skipTests -s settings.xml
- mvn clean install -Dmaven.test.skip=true -s settings.xml


#### MySQL 部署到 K8S
要熟悉配置声明式编程的风格，其实就是看K8S基于yaml格式制定的一些语法规则的编程方式
案例：

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app.kubernetes.io/name: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
```

##### 1、配置和应用分离
- 将数据库名称配置到ConfigMap
  - mysql-cm
- 将数据库密码配置到Secret
  - mysql-secret

##### 2、编写Mysql资源配置清单文件yaml
- mysql-deployment.yaml


