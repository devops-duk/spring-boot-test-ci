#!/bin/bash
## 执行脚本，传入对应的客户端用户名参数，多个用户名空格隔开。
TOKEN="eyJhbGciOiJSUzI1NiIsImtpZCI6ImdwLWlsQU5NX3dndkFza3NQVnpXcW5KRGcycXEyT1U5eUFNQ19mRlJ6eGsifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjNmODU1Mjc1LWM0NDUtNDBiMi1iODBjLWI2MDE3NmQ2MmI3NCIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.B9wqDKGUws3YyozJDwBC59PqmH7u9SATX-tgphn4NMExGGu2XSpbzGRLpcxFFyPLxTi9iREAgke-XFlHTt5ZoMUG_E4ggklknGr7Pe-pXJ-M2jNj1LY0e18yMEUSMPBUBDCaX1GpVP39khEeCEnjrvyI5juW1ehcfezi2f2b3jCvI1qWfh9hoHsb09MdD3I2Q9pEHNrFNaoW4jI_VW9WSIB0Ngg8rCabsMkkp71wmp1XqrUmNNAZjPahs-e0Rs9PNR324IXoMP3QK5lPo-bq1bdPgZ0sIi-Z9Ber2BB1LAIJ3mf2FcemOVuFXM-RwiPULbZSqn6iXZmwnMpYNVUjtg"
APISRRVICE="https://kubernetes.docker.internal"
CLUSTERID="cls-xxxx"
CA_CERT="-----BEGIN CERTIFICATE-----
MIICyDCCAbCgAwIBAgIBADANBgkqhkiG9w0BAQsFADAVMRMwEQYDVQQDEwprdWJl
cm5ldGVzMB4XDTIyMDMxNTEwNDQxNVoXDTMyMDMxMjEwNDQxNVowFTETMBEGA1UE
AxMKa3ViZXJuZXRlczCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALwh
/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAGFzhBWQX0GV75WJKL8PpJ4rpkS/
slgGHdsu6xJ859ZtNfcZrtguJbSb6+GZeqA9tFFNCZRs1ZSn78g+3ja4Whu1HoU9
KRZ7CsOutPacEkHaZP3k95Vdiym+y4wJlFuu7GRmHY6FdOeYPafb5uXqhWv50+Iv
bYslSSzLcygYlYuRwKDqdb0cJBj06y4fZhhQACZ33Y0iTQAJ+CpRnHnu2muqFjIG
YYyFWr8loHu8dT17alcvOVCIdfjXjeEzqpmTqOiIkuZY/zTz3KaXzilxYYU7FhFz
DebKmev/6E0kWj/yrL2knkP1INbtEgRKDUz055sUkMwMhN2gsJHUBq4zfuY=
-----END CERTIFICATE-----"

cat > ./ca.crt <<EOF
${CA_CERT}
EOF

for i in $@ ; do
    # 设置集群参数 --embed-certs为true时表示将certificate-authority证书写入到生成的xxxx.kubeconfig文件中
    kubectl config set-cluster ${CLUSTERID} --certificate-authority=./ca.crt --embed-certs=true --server=${APISRRVICE} --kubeconfig=${i}.kubeconfig

    # 设置客户端认证参数
    kubectl config set-credentials ${i} --token=${TOKEN} --kubeconfig=${i}.kubeconfig

    # 设置上下文参数
    kubectl config set-context ${CLUSTERID}-default --cluster=${CLUSTERID} --user=${i} --kubeconfig=${i}.kubeconfig

    # 设置默认上下文
    kubectl config use-context ${CLUSTERID}-default --kubeconfig=${i}.kubeconfig
done

rm -rf ./ca.crt